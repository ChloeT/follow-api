<?php

namespace App\DataFixtures;

use App\Entity\Entry;
use App\Entity\ToFollow;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User;

        $user->setEmail('mail@mail.fr');
        $user->setPassword('1234');
        $hashPassword = '$argon2id$v=19$m=65536,t=4,p=1$jwYaLfqq4P8zESXk3IKG8w$PCKvBdfzQdeSkcEMoqj3L3lvuVMI/iyix6F7OQ5Z11w';
        $user->setPassword($hashPassword);
        $user->setRoles(["ROLE_USER"]);


        $manager->persist($user);
        $manager->flush();




        $followed1 = new ToFollow;
        $followed1->setName("pain")
            ->setIsIntensity(true)
            ->setIconSlug("fas fa-bolt")
            ->setUser($user);

        $manager->persist($followed1);

        $followed2 = new ToFollow;
        $followed2->setName("flux")
            ->setIsIntensity(true)
            ->setIconSlug("fas fa-tint")
            ->setUser($user);

        $manager->persist($followed2);

        $followed3 = new ToFollow;
        $followed3->setName("emotional")
            ->setIsIntensity(false)
            ->setIconSlug("fas fa-sad-tear")
            ->setUser($user);

        $manager->persist($followed3);




        $entry1 = new Entry;
        $entry1->setUser($user)
            ->setDate(new DateTime())
            ->setFollow($followed1)
            ->setIntensity(3)
            ->setObservation('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

        $manager->persist($entry1);


        $entry2 = new Entry;
        $entry2->setUser($user)
            ->setDate(new DateTime())
            ->setFollow($followed2)
            ->setIntensity(5);

        $manager->persist($entry2);


        $entry3 = new Entry;
        $entry3->setUser($user)
            ->setDate(new DateTime())
            ->setObservation('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

        $manager->persist($entry3);



        $entry4 = new Entry;
        $entry4->setUser($user)
            ->setDate(new DateTime())
            ->setFollow($followed3)
            ->setObservation('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.');

        $manager->persist($entry4);





        $manager->flush();
    }
}
