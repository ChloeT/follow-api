<?php

namespace App\Repository;

use App\Entity\ToFollow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ToFollow|null find($id, $lockMode = null, $lockVersion = null)
 * @method ToFollow|null findOneBy(array $criteria, array $orderBy = null)
 * @method ToFollow[]    findAll()
 * @method ToFollow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ToFollowRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ToFollow::class);
    }

    // /**
    //  * @return ToFollow[] Returns an array of ToFollow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ToFollow
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
