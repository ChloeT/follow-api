<?php

namespace App\Controller;

use App\Entity\ToFollow;
use App\Form\FollowType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/follow")
 */
class ApiFollowController extends AbstractController
{


    /**
     * @Route(methods="GET")
     */
    public function byCurrentUser()
    {
        $user = $this->getUser();

        return $this->json($user->getFollowed());
    }


    

    /**
     * @Route(methods="POST")
     */
    public function newFollow(Request $request, EntityManagerInterface $manager)
    {
        $follow = new ToFollow();
        $form = $this->createForm(FollowType::class, $follow, [
            'csrf_protection' => false
        ]);

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $follow->setUser($this->getUser());

            $manager->persist($follow);
            $manager->flush();

            return $this->json($follow, Response::HTTP_CREATED);
        }

        return $this->json(["message" => "not created " . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }




    /**
     * @Route ("/{id}", methods="GET")
     */
    public function byId(ToFollow $followed)
    {

        return $this->json($followed);
    }
}
