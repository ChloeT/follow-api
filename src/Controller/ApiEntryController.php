<?php

namespace App\Controller;

use App\Entity\Entry;
use App\Form\EntryType;
use App\Repository\ToFollowRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/entry")
 */
class ApiEntryController extends AbstractController
{
    
    

    /**
     * @Route(methods="GET")
     */
    public function byCurrentUser()
    {
        $user = $this->getUser();

        return $this->json($user->getEntries());
    }
    
    

    /**
     * @Route(methods="POST")
     */
    public function newEntry(Request $request, EntityManagerInterface $manager, ToFollowRepository $repoFollow)
    {

        $entry = new Entry();
        $form = $this->createForm(EntryType::class, $entry, [
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
        $form->add('followId', null, ["mapped" => false]);


        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $followId = $form->get("followId")->getData();
            $follow = $repoFollow->findOneBy(['id' => $followId]);

            $entry->setFollow($follow);
            $entry->setUser($this->getUser());
            $entry->setDate(new DateTime());

            $manager->persist($entry);
            $manager->flush();

            return $this->json($entry, Response::HTTP_CREATED);
        }

        return $this->json(["message" => "not created " . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }



}
