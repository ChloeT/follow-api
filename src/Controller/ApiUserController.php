<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api/user")
 */
class ApiUserController extends AbstractController
{


    /**
     * @Route (methods="GET")
     */
    public function currentUser()
    {

        $user = $this->getUser();

        $user->setPassword('');

        return $this->json($user);
    }




    /**
     * @Route(methods="POST")
     */
    public function register(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, [
            'csrf_protection' => false
        ]);

        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setRoles(["ROLE_USER"]);

            $hashedPassword = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hashedPassword);

            $manager->persist($user);
            $manager->flush();

            return $this->json($user, Response::HTTP_CREATED);
        }

        return $this->json(["message" => "user not registred " . $form->getErrors(true)], Response::HTTP_BAD_REQUEST);
    }
}
